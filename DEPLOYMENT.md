## Deployment on Heroku

1. Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
2. Login with `heroku login`
3. Create a new app: `heroku create --stack heroku-20 <your-app-name>` - Skip this if you already have an app created. Also note that heroku stack 20 is used here because this app still runs on Ruby 2. You should upgrade to Ruby 3 if you want to run on latest Heroku stack.
4. Register your app as the remote repository: `heroku git:remote --app <your-app-name>`
5. Add the necessary buildpacks:
```
heroku buildpacks:add --index 1 https://github.com/heroku/heroku-buildpack-activestorage-preview --app <your-app-name>
heroku buildpacks:add --index 2 heroku/nodejs --app <your-app-name>
heroku buildpacks:add --index 3 heroku/ruby --app <your-app-name>
```
6. Set RACK_ENV and HOST: `heroku config:set RACK_ENV=production HOST=production --app <your-app-name>`
7. Deploy the master branch: `git push heroku master`
8. Load database schema: `heroku run rake db:schema:load --app <your-app-name>`
9. Add Heroku redis addons: `heroku addons:create heroku-redis:hobby-dev --app <your-app-name>`. Note that this step is not needed if you already have a redis addon from your paid Heroku plan.
10. Create your community: `heroku run rake db:create_community[your-community-name] --app <your-app-name>`
11. Create a default admin user: `heroku run rake db:create_default_admin[email,username,password] --app <your-app-name>`
12. Add necessary permissions: `heroku run rake db:add_permissions_to_global_roles --app <your-app-name>`
13. Set necessary secret keys as explained [here](https://gitlab.com/doublegdp/open-platform/-/blob/master/HANDBOOK.md#setting-environment-variables). `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` are required to load the app.


## Data Import

To import your data into your new app, you need to follow the steps below:
1. Upload your data to S3 (Let us know if you don't already have your data so we can export it for you)
2. Create an S3 presigned URL of the data so it can be accessed publicly within a set time frame (To be used in the next step).
3. Run the commands below:
```
heroku pg:reset --app <your-app-name>
heroku pg:backups:restore '<s3_presigned_url>' DATABASE_URL --extensions 'pgcrypto,plpgsql,unaccent,uuid-ossp' -a <your-app-name>
```

## Data Update

For some parts of the app to work correctly, the following updates need to be done via Heroku console:
1. Update the community hostname field: `Community.first.update(hostname: 'your-new-domain-name')`
2. Update the community domains field: `Community.first.update(domains: ['your-prod-domain-name', 'your-stagin-domain-name'])`

## Files Migration

We have a script that handles migration of all your uploaded files from our S3 service to your new S3 storage service. To run it, we will need your S3 credentials: `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `BUCKET_NAME` and `region`. Let us know when these are available and we will handle the rest for you.

## Progressive Web Application

If you are setting for a community that DoubleGDP formerly supported then you shouldn't need to do anything to get this setup. If however you are setting up a new community with a different name then you will need update the following:

- Create a directory here `app/assets/images/` that matches your `community.first.name`
- Make sure you have logos in following sizes(in Pixels) and named as following:
    - home_36.png - 36 x 26
    - home_48.png - 48 x 48
    - home_72.png - 72 x 72
    - home_96.png - 96 x 96
    - home_144.png - 144 x 144
    - home_192.png - 192 x 192
    - home_512.png - 512 x 512
- Make sure you are serving your application on a secure connection via https with valid ssl certificates


Note: if you have an issue with any of the above steps, please file an issue here [https://gitlab.com/doublegdp/open-platform/-/issues](https://gitlab.com/doublegdp/open-platform/-/issues)


